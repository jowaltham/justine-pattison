<?php
/**
 * Justine Pattison.
 *
 * This file adds the about page template to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

//* Template Name: About

add_action( 'genesis_entry_content', 'jmw_add_team_members' );
function jmw_add_team_members() {
	global $post;

	$count = get_post_meta( $post->ID, 'team_member', true );
	if( $count ) {

		echo '<div class="team-members">';
			for( $i =0; $i < $count; $i++ ) {
				$image_id = (int) get_post_meta( $post->ID, 'team_member_' . $i . '_profile_picture', true );
				$pname = get_post_meta( $post->ID, 'team_member_' . $i . '_name', true );
				$bio = get_post_meta( $post->ID, 'team_member_' . $i . '_bio', true );
				$image = '';
				if( $image_id ) {
					$image = wp_get_attachment_image( $image_id, 'portrait-medium', '', array( 'class' => 'aligncenter' ) );
				}
				if( $image && $pname && $bio ) {
					echo '<div class="team-member">';
						echo wp_kses_post( $image );
						echo '<h3>' . esc_html( $pname ) . '</h3>';
						echo wpautop( esc_html( $bio ) );
					echo '</div>';
				}
			}
		echo '</div>';
	}
}

//* Run the Genesis loop
genesis();
