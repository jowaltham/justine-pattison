<?php
/**
 * Justine Pattison.
 *
 * This file adds functions to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Setup Theme
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

//* Woo Commerce Includes
include_once( get_stylesheet_directory() . '/lib/woocommerce-integration.php' );

//* Widgets, templates and layouts
include_once( get_stylesheet_directory() . '/lib/widgets-templates-layouts.php' );

//* Helper Functions
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

//* Archive Functions
include_once( get_stylesheet_directory() . '/lib/archive-functions.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'justine-pattison', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'justine-pattison' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', 'Justine Pattison' );
define( 'CHILD_THEME_URL', 'http://www.justinepattison.com/' );
define( 'CHILD_THEME_VERSION', '1.0.9' );

//* Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', 'justine_pattison_enqueue_scripts_styles' );
function justine_pattison_enqueue_scripts_styles() {

	//wp_enqueue_style( 'justine-pattison-fonts', '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700|Lato:300,400,600', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'justine-pattison-fonts', '//fonts.googleapis.com/css?family=Lato:300,400,600', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

}

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

//* Add Accessibility support
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Add support for custom header
add_theme_support( 'custom-header', array(
	'width'           => 1200,
	'height'          => 132,
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'flex-height'     => true,
) );

//* Add Image Sizes
add_image_size( 'portrait-medium', 300, 400, TRUE );
add_image_size( 'landscape-medium', 400, 300, TRUE );
add_image_size( 'square-medium', 240, 240, TRUE );
add_image_size( 'home-profile-pic', 338, 450, TRUE );
add_image_size( 'home-video-pic', 800, 450, TRUE );
add_image_size( 'video-pic', 540, 304, TRUE );

add_filter('image_size_names_choose', 'jmw_add_image_sizes_to_chooser');

/**
 * Add the custom image sizes to the media chooser size dropdown
 */
function jmw_add_image_sizes_to_chooser( $sizes ) {
    $addsizes = array(
        "portrait-medium" => __( "Portrait Medium" ),
        "landscape-medium" => __( "Landscape Medium" ),
    );
    $newsizes = array_merge(  $sizes, $addsizes  );
    return $newsizes;
}

//* Rename primary and secondary navigation menus
add_theme_support( 'genesis-menus' , array( 'primary' => __( 'After Header Menu', 'justine-pattison' ), 'secondary' => __( 'Footer Menu', 'justine-pattison' ), 'my-account' => __( 'My Account', 'justine-pattison' ) ) );

//* Reposition the secondary navigation menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

//* Modify size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'justine_pattison_author_box_gravatar' );
function justine_pattison_author_box_gravatar( $size ) {

	return 90;

}

//* Modify size of the Gravatar in the entry comments
add_filter( 'genesis_comment_list_args', 'justine_pattison_comments_gravatar' );
function justine_pattison_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

//add_filter('genesis_footer_creds_text', 'jmw_customise_credits');
/**
 * Customise the Footer Credits
 */
 function jmw_customise_credits() {

	return '&copy; 2017 - ' . date('Y') . ' Justine Pattison | All Rights Reserved<br />Website by <a href="https://www.calliaweb.co.uk/" rel="nofollow" target="_blank">Callia Web</a>';
}

//* Remove the edit link
add_filter ( 'genesis_edit_post_link' , '__return_false' );


add_filter( 'excerpt_more', 'jmw_excerpt_more' );
//Filter excerpt more
function jmw_excerpt_more( $more ) {
    return '...';
}

add_filter( 'excerpt_length', 'jmw_conditional_excerpt_length', 999 );
// COnditionally change excerpt length
function jmw_conditional_excerpt_length( $length ) {

	if( is_front_page() ) {
		$length = 70;
	}

	return $length;
}



add_filter( 'genesis_post_title_output', 'jmw_screen_reader_text_recipe_entry_titles', 10, 3 );
// Screen reader text the entry title on single recipes
function jmw_screen_reader_text_recipe_entry_titles( $output, $wrap, $title ) {

	if( is_singular( 'jp_recipes' ) ) {

		// Build the output.
		$output = genesis_markup( array(
			'open'    => "<{$wrap} %s>",
			'close'   => "</{$wrap}>",
			'content' => $title,
			'context' => 'screen-reader-text',
			'params'  => array(
				'wrap'  => $wrap,
			),
			'echo'    => false,
		) );
	}

	return $output;
}

add_filter( 'tribe_events_event_classes', 'jmw_remove_feautred_event_class' );
// Remove Featured Event class
function jmw_remove_feautred_event_class( $classes ) {

	$key = array_search( 'tribe-event-featured', $classes );
	if( $key ) {
		unset( $classes[ $key ] );
	}
	return $classes;
}

//add_filter( 'wp_nav_menu_items', 'theme_menu_extras', 10, 2 );
/**
 * Filter menu items, appending either a search form or today's date.
 *
 * @param string   $menu HTML string of list items.
 * @param stdClass $args Menu arguments.
 *
 * @return string Amended HTML string of list items.
 */
function theme_menu_extras( $menu, $args ) {
	//* Change 'primary' to 'secondary' to add extras to the secondary navigation menu
	if ( 'primary' !== $args->theme_location )
		return $menu;

	ob_start();
	get_search_form();
	$search = ob_get_clean();
	$menu  .= '<li class="menu-item search">' . $search . '</li>';

	return $menu;
}

//* Customize search form input box text
add_filter( 'genesis_search_text', 'sp_search_text' );
function sp_search_text( $text ) {
	return esc_attr( 'Search for..' );
}

//add_action( 'admin_menu', 'tuwd_remove_menu_pages' );
// Remove Custom Fields menu option
function tuwd_remove_menu_pages() {
    remove_menu_page('edit.php?post_type=acf-field-group');	//Advanced Custom Fields
}
