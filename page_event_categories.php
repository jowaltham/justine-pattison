<?php
/**
 * This file adds the Events page template
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

/*
Template Name: Justine's Events Page
*/

// tribe events category = tribe_events_cat
// tribe events cpt = tribe_events

add_filter( 'body_class', 'jmw_events_categories_body_class' );
// Add custom body class
function jmw_events_categories_body_class( $classes ) {

	$classes[] = 'grid-archive';

    return $classes;
}

add_action( 'genesis_before_loop', 'jmw_check_for_tribe_events_cat_terms' );
function jmw_check_for_tribe_events_cat_terms() {

	add_action( 'genesis_before_loop', 'jmw_do_events_archive_description', 15 );
	remove_action( 'genesis_loop', 'genesis_do_loop' );
	// Retrieve the terms in Lifestyle Category custom taxonomy
	$tax_terms = get_terms( array(
		'taxonomy' => 'tribe_events_cat',
		'hide_empty' => true
	));
	// if there's at least 1 taxonomy term, output a custom loop
	if ( ! empty( $tax_terms ) && ! is_wp_error( $tax_terms ) ) {
		add_action( 'genesis_loop', 'jmw_do_tribe_events_cat_loop' );
	}
}

function jmw_do_events_archive_description() {

	$headline = genesis_get_cpt_option( 'headline', 'tribe_events' );

	if ( empty( $headline ) && genesis_a11y( 'headings' ) ) {
		$headline = 'Events';
	}

	$intro_text = genesis_get_cpt_option( 'intro_text', 'tribe_events' );

	$headline   = $headline ? sprintf( '<h1 %s>%s</h1>', genesis_attr( 'archive-title' ), strip_tags( $headline ) ) : '';
	$intro_text = $intro_text ? apply_filters( 'genesis_cpt_archive_intro_text_output', $intro_text ) : '';

	if ( $headline || $intro_text )
		printf( '<div %s>%s</div>', genesis_attr( 'cpt-archive-description' ), $headline . $intro_text );
}

/**
 * Outputs a custom loop
 */
function jmw_do_tribe_events_cat_loop() {

	$terms = apply_filters( 'taxonomy-images-get-terms', '', array( 'taxonomy' => 'tribe_events_cat' ) );

	if ( ! empty( $terms ) && !is_wp_error( $terms ) ) {
		print '<div class="events-categories">';
		foreach ( (array) $terms as $term ) { ?>
			<div class="events-category">
				<div class="events-category-image">
					<a href="<?php echo esc_url( get_term_link( $term, $term->taxonomy ) ); ?>" title="<?php printf( __( "View all %s" ), $term->name ); ?>">
					<?php echo wp_get_attachment_image( $term->image_id, 'portrait-medium' ); ?>
						<div class="events-category-content">
							<h2 class="entry-title"><?php echo $term->name; ?></h2>
						</div>
					</a>
				</div>
			</div>
		<?php }
		print '</div>';
	}
}


//* Run the Genesis loop
genesis();
