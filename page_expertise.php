<?php
/**
 * Justine Pattison.
 *
 * This file adds the expertise page template to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

//* Template Name: Expertise

add_action( 'genesis_entry_content', 'jmw_add_expertise_sub_pages' );
function jmw_add_expertise_sub_pages() {
	global $post;

	$count = get_post_meta( $post->ID, 'expertise_sub_pages', true );
	if( $count ) {

		echo '<div class="expertise-sub-pages">';
			for( $i =0; $i < $count; $i++ ) {
				$page_id = get_post_meta( $post->ID, 'expertise_sub_pages_' . $i . '_sub_page', true );

				
				$image = get_the_post_thumbnail( $page_id, 'portrait-medium', array( 'class' => 'aligncenter' ) );
				$title = get_the_title( $page_id );
				$link = get_the_permalink( $page_id );

				if( $image && $title && $link ) {
					echo '<div class="sub-page">';
						echo '<a href="' . esc_url( $link ) . '">';
							echo wp_kses_post( $image );
							echo '<h2>' . esc_html( $title ) . '</h2>';
						echo '</a>';
					echo '</div>';
				}
			}
		echo '</div>';
	}
}

//* Run the Genesis loop
genesis();
