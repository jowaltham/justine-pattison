<?php
/**
 * This file adds the Video page template
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

/*
Template Name: Video Page
*/

//* Add single page body class to the head
//add_filter( 'body_class', 'jmw_add_single_body_class' );

add_action( 'genesis_entry_content', 'jmw_add_video_custom_fields' );
function jmw_add_video_custom_fields() {
	global $post;

	$nvideos = get_post_meta( $post->ID, 'videos', true );

	if( $nvideos ) {
		for( $i = 0; $i < $nvideos; $i++ ) {

			$title_text = get_post_meta( $post->ID, 'videos_' . $i . '_title', true );
			$description = get_post_meta( $post->ID, 'videos_' . $i . '_description', true );
			$type_video = get_post_meta( $post->ID, 'videos_' . $i . '_type_of_content', true );

			$video_output = '';

			if( $title_text || $description ) {
				if( 'video' === $type_video ) {
					$video_embed_url = get_post_meta( $post->ID, 'videos_' . $i . '_video_embed_url', true );
					if( $video_embed_url ) {
						if( shortcode_exists( 'fve' ) ) {
							$video_output = '[fve]' . esc_url( $video_embed_url ) . '[/fve]';
						} else {
							$video_output = wp_oembed_get( esc_url( $video_embed_url ) );
						}
					}
				}
				else if ( 'image' === $type_video ) {
					$image_id = (int) get_post_meta( $post->ID, 'videos_' . $i . '_image', true );
					$image = wp_get_attachment_image( $image_id, 'video-pic', false, array( "class" => 'aligncenter') );
					$video_url = get_post_meta( $post->ID, 'videos_' . $i . '_video_url', true );
					if( $image && $video_url ) {
						$video_output = sprintf( '<p><a href="%s">%s</a></p><p class="aligncenter"><a class="button" href="%s">Watch now</a></p>',
							esc_url( $video_url ),
							$image,
							esc_url( $video_url )
						);
					}
				}

				if( !empty( $video_output ) ) { ?>
					<div class="video">
						<p><?php echo do_shortcode( $video_output ); ?></p>
						<h2><?php echo esc_html( $title_text ); ?></h2>
						<?php echo wp_kses_post( wpautop( $description ) ); ?>
					</div>
				<?php }
			}
		}
	}
}


//* Run the Genesis loop
genesis();
