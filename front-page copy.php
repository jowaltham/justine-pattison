<?php
/**
 * Justine Pattison.
 *
 * This file adds the front page template to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

// Make site title h1 for the front page
add_filter( 'genesis_site_title_wrap', 'jmw_h1_for_site_title' );
function jmw_h1_for_site_title( $wrap ) {
	return 'h1';
}

//* Remove the entry header and title
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

// Add custom fields
add_action( 'genesis_entry_content', 'jmw_add_home_page_custom_fields' );
function jmw_add_home_page_custom_fields() {
	global $post;

	// Initialise some variables
	$profile_image = $video_output = $topics_output = $featured_images_output = $product_cat_image = $product_cat_output = $kitchen_image = '';
	$topic_output_array = $featured_images_array = $product_cat_output_array = array();

	$profile_image_id = (int) get_post_meta( $post->ID, 'profile_image', TRUE );
	if( $profile_image_id ) {
		$profile_image = wp_get_attachment_image( $profile_image_id, 'home-profile-pic', false, array( "class" => 'touching') );
	}

	$intro_video_url = get_post_meta( $post->ID, 'intro_video', TRUE );
	if( $intro_video_url ) {
		if( shortcode_exists( 'fve' ) ) {
			$video_output = do_shortcode( '[fve]' . esc_url( $intro_video_url) . '[/fve]' );
		} else {
			$video_output = wp_oembed_get( esc_url( $intro_video_url ) );
		}
	} else {
		// we have no video so will use the replacement image
		$video_replacement_image_id = (int) get_post_meta( $post->ID, 'video_replacement_image', TRUE );
		if( $video_replacement_image_id ) {
				$video_output = wp_get_attachment_image( $video_replacement_image_id, 'home-video-pic', false, array( "class" => 'touching') );
		}
	}



	$bio = get_post_meta( $post->ID, 'bio', TRUE );

	// 3 Topics
	$ntopics = get_post_meta( $post->ID, 'topics', TRUE );
	if( $ntopics ) {
		for( $i = 0; $i < $ntopics; $i++ ) {
			$topic_title = get_post_meta( $post->ID, 'topics_' . $i . '_title', true );

			//$topic_page_id = (int) get_post_meta( $post->ID, 'topics_' . $i . '_topic_page', true );
			$topic_page_url = get_post_meta( $post->ID, 'topics_' . $i . '_topic_page', true );
			//$topic_page = get_permalink( $topic_page_id );
			$topic_intro_text = get_post_meta( $post->ID, 'topics_' . $i . '_intro_text', true );
			$topic_image_id = (int) get_post_meta( $post->ID, 'topics_' . $i . '_topic_image', true );
			if( $topic_image_id ) {
				$topic_image = wp_get_attachment_image( $topic_image_id, 'portrait-medium', false, array( "class" => 'aligncenter') );
			}
			$topic_output_array[] = sprintf( '<div class="topic"><a class="grey" href="%s"><h2>%s</h2>%s%s</a></div>',
				esc_url( $topic_page_url ),
				esc_html( $topic_title ),
				$topic_image,
				wpautop( esc_html( $topic_intro_text ))
			);
		}
		if( !empty( $topic_output_array ) ) {
			$topics_output = implode( $topic_output_array );
		}
	}


	$kitchen_image_id = get_post_meta( $post->ID, 'test_kitchen_image', true);
	$kitchen_title = get_post_meta( $post->ID, 'test_kitchen_title', true);
	$kitchen_link = get_post_meta( $post->ID, 'test_kitchen_link', true);
	$kitchen_logos = get_post_meta( $post->ID, 'test_kitchen_logos', true);

	if ($kitchen_image_id) {
		$kitchen_image = wp_get_attachment_image( $kitchen_image_id, 'large', false, array( "class" => 'aligncenter') );
	}

	if( $kitchen_logos ) {
		for( $i = 0; $i < $kitchen_logos; $i++ ) {
			$image_id = (int) get_post_meta( $post->ID, 'test_kitchen_logos_' . $i . '_image', true );
			if( $image_id ) {
				$kitchen_logos_array[] = wp_get_attachment_image( $image_id, 'square-medium', false, array( "class" => 'kitchen-logo') );
			}
		}
		if( !empty( $kitchen_logos_array ) ) {
			$kitchen_logos_output = implode( '', $kitchen_logos_array );
		}
	}

	$article_section_title = get_post_meta( $post->ID, 'article_section_title', TRUE );

	$nimages = get_post_meta( $post->ID, 'featured_images', TRUE );
	if( $nimages ) {
		for( $i = 0; $i < $nimages; $i++ ) {
			$image_id = (int) get_post_meta( $post->ID, 'featured_images_' . $i . '_image', true );
			if( $image_id ) {
				$featured_images_array[] = wp_get_attachment_image( $image_id, 'square-medium', false, array( "class" => 'touching') );
			}
		}
		if( !empty( $featured_images_array ) ) {
			$featured_images_output = implode( $featured_images_array );
		}
	}

	// 3 Product Categories
	$nproduct_cat = get_post_meta( $post->ID, 'featured_product_categories', TRUE );
	if( $nproduct_cat ) {
		for( $i = 0; $i < $nproduct_cat; $i++ ) {
			$product_cat_id = get_post_meta( $post->ID, 'featured_product_categories_' . $i . '_category', true );
			$product_term = get_term( $product_cat_id, 'product_cat' );
			$product_cat_image_id = (int) get_post_meta( $post->ID, 'featured_product_categories_' . $i . '_image', true );
			$product_cat_title = get_post_meta( $post->ID, 'featured_product_categories_' . $i . '_title', true );
			$product_cat_intro_text = get_post_meta( $post->ID, 'featured_product_categories_' . $i . '_intro_text', true );
			if( $product_cat_image_id ) {
				$product_cat_image = wp_get_attachment_image( $product_cat_image_id, 'portrait-medium', false, array( "class" => 'aligncenter') );
			}
			if( !is_wp_error( $product_term ) ) {
				$product_cat_output_array[] = sprintf( '<div class="product-category"><a class="grey" href="%s"><h2>%s</h2>%s%s</a></div>',
					esc_url( get_term_link( $product_term ) ),
					esc_html( $product_cat_title ),
					$product_cat_image,
					wpautop( esc_html( $product_cat_intro_text ) )
				);
			}
		}
		if( !empty( $product_cat_output_array ) ) {
			$product_cat_output = implode( '', $product_cat_output_array );
		}
	} ?>

	<!-- Output -->
	<div class="welcome">
		<div class="home-profile">
			<?php echo $profile_image; ?>
		</div>
		<div class="home-video">
			<?php echo $video_output; ?>
		</div>
	</div>
	<div class="bio">
		<?php echo esc_html( $bio ); ?>
	</div>
	<div class="topics">
	<?php echo $topics_output; ?>
	</div>
	<div class="featured-products">
		<?php echo $product_cat_output;  ?>
	</div>
	<div class="test-kitchen">
		<?php
		if ($kitchen_title) {
			if ($kitchen_link) {
				printf( '<h2 class="test-kitchen-title aligncenter"><a href="%s">%s</a></h2>', esc_url($kitchen_link), esc_html( $kitchen_title ) );
			}
			else {
				printf('<h2 class="test-kitchen-title aligncenter">%s</h2>', esc_html($kitchen_title));
			}
		}
		if ($kitchen_image && $kitchen_link) {
			printf('<a href="%s">%s</a>', esc_url($kitchen_link), $kitchen_image);
		}

		if ($kitchen_logos_output) {
			echo '<div class="test-kitchen-logos">';
			echo $kitchen_logos_output;
			echo '</div>';
		}

		/*if( $kitchen_text && $kitchen_link ) {
			printf( '<p class="aligncenter"><a href="%s" class="button">%s</a></p>', esc_url($kitchen_link), esc_html( $kitchen_text ) );
		}*/

		?>
	</div>

	<<div class="article-section">
		<h2><?php echo esc_html( $article_section_title ); ?></h2>
		<?php $article_args = array(
			'post_type' => 'post',
			'posts_per_page' => 1,
		);
		$articles = new WP_Query( $article_args );
		while( $articles->have_posts() ) {
			$articles->the_post(); ?>
			<div class="latest-article">
				<a class="dark-grey" href="<?php the_permalink(); ?>">
					<div class="article-image">
						<?php the_post_thumbnail( 'portrait-medium', array( 'class' => 'aligncenter' ) ); ?>
					</div>
					<div class="article-text">
						<h3><?php the_title(); ?></h3>
						<?php wpautop( the_excerpt() ); ?>
						<p><span class="button">Read More</span></p>
					</div>
				</a>
			</div>
		<?php }
 		wp_reset_postdata(); ?>
	</div>

	<div class="featured-images">
		<?php echo $featured_images_output; ?>
	</div>

 	<?php genesis_widget_area( 'subscribe-widget-area', array(
			'before' => '<div class="subscribe-widget-area widget-area"><div class="wrap">',
			'after' => '</div></div>',
	) ); ?>

	<?php
}

//* Run the Genesis loop
genesis();
