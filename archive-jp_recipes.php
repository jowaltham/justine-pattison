<?php
/**
 * Justine Pattison.
 *
 * This file adds the expertise archive template to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

add_action( 'genesis_before_loop', 'jmw_check_for_recipe_terms' );
function jmw_check_for_recipe_terms() {

	// Retrieve the terms in Recipe Category custom taxonomy
	$tax_terms = get_terms( 'jp_recipe_category' );

	// if there's at least 1 taxonomy term, replace the default loop with a custom one
	if ( ! empty( $tax_terms ) && ! is_wp_error( $tax_terms ) ) {
		remove_action( 'genesis_loop', 'genesis_do_loop' );
		add_action( 'genesis_loop', 'jmw_do_recipe_loop' );
	}
}

/**
 * Outputs a custom loop
 */
function jmw_do_recipe_loop() {

    $terms = apply_filters( 'taxonomy-images-get-terms', '', array( 'taxonomy' => 'jp_recipe_category' ) );

    if ( ! empty( $terms ) && !is_wp_error( $terms ) ) {
        print '<div class="recipe-categories">';
        foreach ( (array) $terms as $term ) { ?>
            <div class="recipe-category">
                <div class="recipe-image">
                    <a href="<?php echo esc_url( get_term_link( $term, $term->taxonomy ) ); ?>" title="<?php printf( __( "View all %s" ), $term->name ); ?>">
                    <?php echo wp_get_attachment_image( $term->image_id, 'portrait-medium' ); ?>
                        <div class="recipe-content">
                            <h2 class="entry-title"><?php echo $term->name; ?></h2>
                        </div>
                    </a>
                </div>
            </div>
        <?php }
        print '</div>';
    }

}

//* Run the Genesis loop
genesis();
