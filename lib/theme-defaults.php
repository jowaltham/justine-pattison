<?php
/**
 * Justine Pattison.
 *
 * This file adds the default theme settings to the Justine Pattison Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

add_filter( 'genesis_theme_settings_defaults', 'justine_pattison_defaults' );
/**
* Updates theme settings on reset.
*
* @since 2.2.3
*/
function justine_pattison_defaults( $defaults ) {

	$defaults['blog_cat_num']              = 6;
	$defaults['content_archive']           = 'excerpts';
	$defaults['content_archive_thumbnail'] = 1;
	$defaults['image_size']                = 'medium';
	$defaults['image_alignment']           = 'alignleft';
	$defaults['posts_nav']                 = 'numeric';
	$defaults['site_layout']               = 'content-sidebar';

	return $defaults;

}

add_action( 'after_switch_theme', 'justine_pattison_setting_defaults' );
/**
* Updates theme settings on activation.
*
* @since 2.2.3
*/
function justine_pattison_setting_defaults() {

	if ( function_exists( 'genesis_update_settings' ) ) {

		genesis_update_settings( array(
			'blog_cat_num'              => 6,
			'content_archive'           => 'excerpts',
			'content_archive_thumbnail' => 1,
			'image_size'				=> 'medium',
			'image_alignment'			=> 'alignleft',
			'posts_nav'                 => 'numeric',
			'site_layout'               => 'content-sidebar',
		) );

	}

	update_option( 'posts_per_page', 6 );

}