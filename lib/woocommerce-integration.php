<?php
/**
 * Justine Pattison
 *
 * This file adds the WooCommerce integration to the Cambridge Food Collective Theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

// Add WooCommerce support for Genesis layouts (sidebar, full-width, etc)
add_post_type_support( 'product', 'genesis-layouts' );

// Unhook WooCommerce Sidebar - use Genesis Sidebars instead
remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

// Unhook WooCommerce wrappers
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

// Hook new functions with Genesis wrappers
add_action( 'woocommerce_before_main_content', 'jmw_my_theme_wrapper_start', 10 );
add_action( 'woocommerce_after_main_content', 'jmw_my_theme_wrapper_end', 10 );

// Add opening wrapper before WooCommerce loop
function jmw_my_theme_wrapper_start() {

    do_action( 'genesis_before_content_sidebar_wrap' );
    genesis_markup( array(
        'html5' => '<div %s>',
        'xhtml' => '<div id="content-sidebar-wrap">',
        'context' => 'content-sidebar-wrap',
    ) );

    do_action( 'genesis_before_content' );
    genesis_markup( array(
        'html5' => '<main %s>',
        'xhtml' => '<div id="content" class="hfeed">',
        'context' => 'content',
    ) );
    do_action( 'genesis_before_loop' );
}

/* Add closing wrapper after WooCommerce loop */
function jmw_my_theme_wrapper_end() {

    do_action( 'genesis_after_loop' );
    genesis_markup( array(
        'html5' => '</main>', //* end .content
        'xhtml' => '</div>', //* end #content
    ) );
    do_action( 'genesis_after_content' );

    echo '</div>'; //* end .content-sidebar-wrap or #content-sidebar-wrap
    do_action( 'genesis_after_content_sidebar_wrap' );

}

// Remove WooCommerce breadcrumbs
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

/*
 * WooCommerce Customisations
 */

// Remove WooCommerce Theme Support admin message
add_theme_support( 'woocommerce' );

// Remove product category count
add_filter( 'woocommerce_subcategory_count_html', '__return_false' );

// Remove SKUs from the front end
add_filter( 'wc_product_sku_enabled', '__return_false' );


// Remove Additional Information Tab
add_filter( 'woocommerce_product_tabs', 'jmw_remove_product_tabs', 98 );
function jmw_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );
    return $tabs;
}

// remove default sorting dropdown
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Remove Woo Commerce Page title on archives and use Genesis one instead
add_filter( 'woocommerce_show_page_title', 'jmw_remove_woo_page_title_except_for_shop_page' );
function jmw_remove_woo_page_title_except_for_shop_page( $show ) {

    // return true if its the shop page but false otherwise
    return is_shop();

}

add_filter( 'woocommerce_cart_item_remove_link', 'jmw_add_screen_reader_text_to_remove_from_cart_link', 10, 2 );
function jmw_add_screen_reader_text_to_remove_from_cart_link( $remove_link, $cart_item_key ) {
    $item_data = WC()->cart->get_cart_item( $cart_item_key );
    $title = $item_data['data']->post->post_title;

    $screen_reader_text = sprintf( '<span class="screen-reader-text">remove %s from basket</span></a>', $title );

    $remove_link = str_replace( '</a>', $screen_reader_text, $remove_link );

    return $remove_link;
}

add_filter( 'yith_woocommerce_gift_cards_add_to_cart_text', 'jmw_change_gift_card_add_to_cart_text' );
function jmw_change_gift_card_add_to_cart_text( $text ) {
    return 'Add to basket';
}

// Change external product links to open in new tab
add_filter( 'woocommerce_loop_add_to_cart_link', 'jmw_loop_external_products_open_new_tab', 10, 2 );
function jmw_loop_external_products_open_new_tab( $html, $product ) {
    global $product;

    if ( !$product->is_type( 'external' ) ) {
        return $html;
    }
    $html = sprintf( '<a rel="nofollow" target="_blank" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
        esc_url( $product->add_to_cart_url() ),
        esc_attr( isset( $quantity ) ? $quantity : 1 ),
        esc_attr( $product->get_id() ),
        esc_attr( $product->get_sku() ),
        esc_attr( isset( $class ) ? $class : 'button' ),
        esc_html( $product->add_to_cart_text() )
    );

    return $html;
}

add_filter( 'woocommerce_loop_add_to_cart_link', 'jmw_change_add_to_cart_on_archive', 10, 2 );
function jmw_change_add_to_cart_on_archive( $add_to_cart, $product ) {

    return  sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">Find Out More</a>',
        esc_url( get_permalink( $product->get_id() ) ),
        esc_attr( isset( $quantity ) ? $quantity : 1 ),
        esc_attr( $product->get_id() ),
        esc_attr( $product->get_sku() ),
        esc_attr( isset( $class ) ? $class : 'button' )
    );

}

