<?php
/**
 * Justine Pattison.
 *
 * This file alters the default widgets, templates and layouts
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */


/*
 * Widgets
 */

//* Add support for after entry widget
add_theme_support( 'genesis-after-entry-widget-area' );

//* Add support for 2 footer widgets
add_theme_support( 'genesis-footer-widgets', 2 );

// Register widgets
genesis_register_widget_area( array(
    'id'            => 'above-header-one',
    'name'          => __( 'Above Header One', 'justine-pattison' ),
    'description'   => __( 'This is a widget area for the social media icons and customer menu', 'justine-pattison' ),
) );
genesis_register_widget_area( array(
    'id'            => 'above-header-two',
    'name'          => __( 'Above Header Two', 'justine-pattison' ),
    'description'   => __( 'This is a widget area for the header contact information', 'justine-pattison' ),
) );
genesis_register_widget_area( array(
    'id'            => 'subscribe-widget-area',
    'name'          => __( 'Subscribe Widget Area', 'justine-pattison' ),
    'description'   => __( 'This is a widget area for the subscribe widget', 'justine-pattison' ),
) );

// Display above header widgets
add_action( 'genesis_before_header', 'jmw_show_above_header_widget_areas' );
function jmw_show_above_header_widget_areas() {
    genesis_widget_area( 'above-header-one', array(
            'before' => '<div class="above-header-one widget-area"><div class="wrap">',
            'after' => '</div></div>',
    ) );
    genesis_widget_area( 'above-header-two', array(
            'before' => '<div class="above-header-two widget-area"><div class="wrap">',
            'after' => '</div></div>',
    ) );
}

// Add after entry widget to posts and pages
add_action( 'genesis_after_entry', 'jmw_after_entry_cpt', 9 );
function jmw_after_entry_cpt() {

   if ( is_singular( array( 'jp_recipes', 'jp_expertise' )) ) {
    
        genesis_widget_area( 'after-entry', array(
            'before' => '<div class="after-entry widget-area">',
            'after'  => '</div>',
        ) );
    }
}

// Remove wrap from footer-widgets
add_theme_support( 'genesis-structural-wraps', array( 'header', 'menu-primary', 'menu-secondary', 'footer' ) );

/*
 * Templates
 * http://www.billerickson.net/remove-genesis-page-templates
 */
function be_remove_genesis_page_templates( $page_templates ) {
    unset( $page_templates['page_archive.php'] );
    unset( $page_templates['page_blog.php'] );
    return $page_templates;
}
add_filter( 'theme_page_templates', 'be_remove_genesis_page_templates' );


/*
 * Layouts
 */

//* Unregister unused sidebars and layouts
unregister_sidebar( 'sidebar-alt' );
unregister_sidebar( 'header-right' );
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

add_action( 'genesis_meta', 'jmw_set_layouts' );
function jmw_set_layouts() {

    if( is_singular( 'post' ) ) {
        add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_content_sidebar' );
    }
    else {
        add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
    }

}