<?php
/**
 * Justine Pattison.
 *
 * This file adds helper functions used elsewhere in the theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

function jmw_custom_excerpt() {

    printf('<a class="grey" href="%s">%s</a><br/><br/><p><a class="button" href="%s">Read More <span class="screen-reader-text">about %s</span></a></p>',
            get_the_permalink(),
            get_the_excerpt(),
            get_the_permalink(),
            get_the_title()
        );

}