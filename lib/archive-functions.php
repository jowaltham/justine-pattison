<?php
/**
 * Justine Pattison.
 *
 * This file alters the archive functions in the theme.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

add_filter( 'pre_get_posts', 'be_archive_query' );
/**
 * Archive Query
 *
 * Sets all custom taxonomy archives to 12 per page
 * @since 1.0.0
 * @link http://www.billerickson.net/customize-the-wordpress-query/
 *
 * @param object $query
 */
function be_archive_query( $query ) {
	if( $query->is_main_query() && $query->is_tax() ) {
		$query->set( 'posts_per_page', 12 );
	}
}

add_filter( 'body_class', 'jmw_cpt_tax_body_class' );
// Add custom body class
function jmw_cpt_tax_body_class( $classes ) {

	if( is_post_type_archive()  || is_tax() ) {
		$classes[] = 'grid-archive';
	} else if( is_archive() || is_home() ) {
		$classes[] = 'stacked-archive';
	}

    return $classes;
}

add_filter( 'genesis_options', 'jmw_set_recipe_category_genesis_options' );
// Set archive image size and alignment
function jmw_set_recipe_category_genesis_options( $args ) {

	if( is_tax() ) {
		$args['image_alignment'] = 'aligncenter';
		$args['image_size']  = 'portrait-medium';
	}

    return $args;
}

// Remove header post meta from everywhere including single posts
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Customize the entry meta in the entry footer
add_filter( 'genesis_post_meta', 'sp_post_meta_filter' );
function sp_post_meta_filter($post_meta) {

	if( 'post' === get_post_type() ) {
		$post_meta = '[post_date]<br />[post_categories before="Category: "]';
	}
	else if ( 'jp_expertise' == get_post_type() ) {
		$post_meta = get_the_term_list( get_the_ID(), 'jp_expertise_topic', 'Category: ', ', ' );
	}
	else if ( 'jp_recipes' == get_post_type() ) {
		$post_meta = get_the_term_list( get_the_ID(), 'jp_recipe_category', 'Category: ', ', ' );
	}
	return $post_meta;
}

add_action( 'genesis_before_loop', 'jmw_customise_archive_loops' );
// customised archive content
function jmw_customise_archive_loops() {

	if( is_archive() || is_home() ) {

		// Remove post content
		remove_action('genesis_entry_content', 'genesis_do_post_content' );

		// Relocate image
		remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );
		add_action( 'genesis_entry_header', 'genesis_do_post_image', 8 );

	}

	if( is_tax() ) {
		// Remove footer post meta
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_open', 5 );
		remove_action( 'genesis_entry_footer', 'genesis_post_meta' );
		remove_action( 'genesis_entry_footer', 'genesis_entry_footer_markup_close', 15 );
	}

	if( is_category() || is_tag() || is_author() || is_date() || is_home() ) {

		// Add our custom excerpt
		add_action( 'genesis_entry_content', 'jmw_custom_excerpt' );

	}
}