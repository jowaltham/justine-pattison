<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

?>

<?php echo tribe_event_featured_image( null, 'portrait-medium' ) ?>


<?php do_action( 'tribe_events_before_the_event_title' ) ?>

<h2 class="tribe-events-list-event-title entry-title">
	<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
		<?php the_title() ?>
	</a>
</h2>

<?php do_action( 'tribe_events_after_the_event_title' ) ?>

<?php do_action( 'tribe_events_before_the_meta' ) ?>
<div class="tribe-events-event-meta">

	<div class="tribe-event-schedule-details">
		<?php echo tribe_events_event_schedule_details() ?>
	</div>

	<?php if ( tribe_get_cost() ) : ?>
		<div class="tribe-events-event-cost">
			<span><?php echo tribe_get_cost( null, true ); ?></span>
		</div>
	<?php endif; ?>
	<div class="book-now">
		<a class="button" href="<?php echo esc_url( tribe_get_event_link() ); ?>">Book Now<span class="screen-reader-text"> on <?php the_title(); ?></span></a>
	</div>
</div>


<?php do_action( 'tribe_events_after_the_meta' ) ?>

<?php do_action( 'tribe_events_after_the_content' );