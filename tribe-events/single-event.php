<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

?>

<div id="tribe-events-content" class="tribe-events-single">

	<!-- Notices -->
	<?php tribe_the_notices() ?>



	<!-- #tribe-events-header -->

	<?php while ( have_posts() ) :  the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<!-- Event featured image, but exclude link -->
			<?php echo tribe_event_featured_image( $event_id, 'portrait-medium', false ); ?>

			<div class="jp-event-meta">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

				<?php echo tribe_events_event_schedule_details( $event_id, '', '<br />' ); ?>
				<?php if ( tribe_get_cost() ) : ?>
					<span class="tribe-events-cost"><?php echo tribe_get_cost( null, true ) ?></span><br />
				<?php endif; ?>
				<?php
				echo tribe_get_event_categories(
					get_the_id(), array(
						'before'       => '',
						'sep'          => ', ',
						'after'        => '',
						'label'        => 'Category', // An appropriate plural/singular label will be provided
						'label_before' => '',
						'label_after'  => '',
						'wrap_before'  => '',
						'wrap_after'   => '',
					)
				);

				//echo $ticket->remaining();
			?>


			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
		</div>
		<div class="clearfix"></div>
			<!-- Event content -->

			<div class="tribe-events-single-event-description entry-content">
				<?php the_content(); ?>
			</div>

			<!-- .tribe-events-single-event-description -->
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

			<!-- Event meta -->
			<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>

			<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
		</div> <!-- #post-x -->
		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>


</div><!-- #tribe-events-content -->
