<?php
/**
 * Justine Pattison recipe template.
 *
 * @package Justine Pattison
 * @author  Callia Web
 * @license GPL-2.0+
 * @link    https://www.calliaweb.co.uk/
 */

// @codingStandardsIgnoreStart
?>
<div class="wprm-recipe wprm-recipe-justine-pattison" itemscope itemtype="http://schema.org/Recipe">
	<meta itemprop="author" content="<?php echo $recipe->author_meta(); ?>" />
	<meta itemprop="datePublished" content="<?php echo $recipe->date(); ?>" />
	<meta itemprop="image" content="<?php echo $recipe->image_url( 'full' ); ?>" />
	<div class="wprm-recipe-name" itemprop="name"><h2 class="entry-title"><?php echo $recipe->name(); ?></h2></div>
	<div class="wprm-recipe-image-container">
		<?php
		if ( $recipe->has_rating() ) {
			echo $recipe->rating_stars( true );
		}
		?>
		<div class="wprm-recipe-image"><?php echo $recipe->image( 'portrait-medium' ); ?></div>
		<div class="wprm-recipe-buttons">
			<a href="#" class="wprm-recipe-print button"><?php echo WPRM_Template_Helper::label( 'print_button' ); ?></a>
		</div>
	</div>
	<?php if ( $recipe->prep_time() || $recipe->prep_time() || $recipe->prep_time() ) : ?>
	<div class="wprm-recipe-times-container wprm-color-border">
		<?php if ( $recipe->prep_time() ) : ?>
		<div class="wprm-recipe-time-container wprm-color-border">
			<meta itemprop="prepTime" content="PT<?php echo $recipe->prep_time(); ?>M" />
			<div class="wprm-recipe-time-header"><strong><?php echo WPRM_Template_Helper::label( 'prep_time' ); ?></strong></div>
			<div class="wprm-recipe-time"><?php echo $recipe->prep_time_formatted( true ); ?></div>
		</div>
		<?php endif; // Prep time. ?>
		<?php if ( $recipe->cook_time() ) : ?>
			<div class="wprm-recipe-time-container wprm-color-border">
				<meta itemprop="cookTime" content="PT<?php echo $recipe->cook_time(); ?>M" />
				<div class="wprm-recipe-time-header"><strong><?php echo WPRM_Template_Helper::label( 'cook_time' ); ?></strong></div>
				<div class="wprm-recipe-time"><?php echo $recipe->cook_time_formatted( true ); ?></div>
			</div>
		<?php endif; // Cook time. ?>
		<?php if ( $recipe->total_time() ) : ?>
			<div class="wprm-recipe-time-container wprm-color-border">
				<meta itemprop="totalTime" content="PT<?php echo $recipe->total_time(); ?>M" />
				<div class="wprm-recipe-time-header"><strong><?php echo WPRM_Template_Helper::label( 'calories' ); ?></strong></div>
				<div class="wprm-recipe-time"><?php echo $recipe->calories(); ?></div>
			</div>
		<?php endif; // Total time. ?>
		<div class="wprm-clear-left">&nbsp;</div>
	</div>
	<?php endif; // Recipe times. ?>
	<div class="wprm-recipe-summary" itemprop="description">
		<?php echo $recipe->summary(); ?>
	</div>
	<div class="wprm-recipe-details-container">
		<?php
		$taxonomies = WPRM_Taxonomies::get_taxonomies();

		foreach ( $taxonomies as $taxonomy => $options ) :
			$key = substr( $taxonomy, 5 );
			$terms = $recipe->tags( $key );

			if ( count( $terms ) > 0 ) : ?>
			<div class="wprm-recipe-<?php echo $key; ?>-container">
				<span class="wprm-recipe-details-name wprm-recipe-<?php echo $key; ?>-name"><strong><?php echo WPRM_Template_Helper::label( $key . '_tags', $options['singular_name'] ); ?></strong>:</span>
				<span class="wprm-recipe-<?php echo $key; ?>"<?php echo WPRM_Template_Helper::tags_meta( $key ); ?>>
					<?php foreach ( $terms as $index => $term ) {
						if ( 0 !== $index ) {
							echo ', ';
						}
						echo $term->name;
					} ?>
				</span>
			</div>
		<?php endif; // Count.
		endforeach; // Taxonomies. ?>
		<?php if ( $recipe->servings() ) : ?>
		<div class="wprm-recipe-servings-container">
			<span class="wprm-recipe-details-name wprm-recipe-servings-name"><strong><?php echo WPRM_Template_Helper::label( 'servings' ); ?></strong></span>: <span itemprop="recipeYield"><span class="wprm-recipe-details wprm-recipe-servings"><?php echo $recipe->servings(); ?></span> <span class="wprm-recipe-details-unit wprm-recipe-servings-unit"><?php echo $recipe->servings_unit(); ?></span></span>
		</div>
		<?php endif; // Servings. ?>
		<?php if ( $recipe->calories() ) : ?>
		<div class="wprm-recipe-calories-container" itemprop="nutrition" itemscope itemtype="http://schema.org/NutritionInformation">
			<span class="wprm-recipe-details-name wprm-recipe-calories-name"><strong><?php echo WPRM_Template_Helper::label( 'calories' ); ?></strong></span>: <span itemprop="calories"><span class="wprm-recipe-details wprm-recipe-calories"><?php echo $recipe->calories(); ?></span> <span class="wprm-recipe-details-unit wprm-recipe-calories-unit"><?php _e( 'kcal', 'wp-recipe-maker' ); ?></span></span>
		</div>
		<?php endif; // Calories. ?>
	</div>

	<?php
	$ingredients = $recipe->ingredients();
	if ( count( $ingredients ) > 0 ) : ?>
	<div class="wprm-recipe-ingredients-container">
		<div class="wprm-recipe-header"><h3><?php echo WPRM_Template_Helper::label( 'ingredients' ); ?></h3></div>
		<?php foreach ( $ingredients as $ingredient_group ) : ?>
		<div class="wprm-recipe-ingredient-group">
			<?php if ( $ingredient_group['name'] ) : ?>
			<div class="wprm-recipe-group-name wprm-recipe-ingredient-group-name"><h4><?php echo $ingredient_group['name']; ?></h4></div>
			<?php endif; // Ingredient group name. ?>
			<ul class="wprm-recipe-ingredients">
				<?php foreach ( $ingredient_group['ingredients'] as $ingredient ) : ?>
				<li class="wprm-recipe-ingredient" itemprop="recipeIngredient">
					<?php if ( $ingredient['amount'] ) : ?>
					<span class="wprm-recipe-ingredient-amount"><?php echo $ingredient['amount'], $ingredient['unit']; ?></span>
					<?php endif; // Ingredient amount. ?>
					<span class="wprm-recipe-ingredient-name"><?php echo WPRM_Template_Helper::ingredient_name( $ingredient, true ); ?></span>
					<?php if ( $ingredient['notes'] ) : ?>
					<span class="wprm-recipe-ingredient-notes"><?php echo $ingredient['notes']; ?></span>
					<?php endif; // Ingredient notes. ?>
				</li>
				<?php endforeach; // Ingredients. ?>
			</ul>
		</div>
	 <?php endforeach; // Ingredient groups. ?>
	</div>
	<?php endif; // Ingredients. ?>
	<?php
	$instructions = $recipe->instructions();
	if ( count( $instructions ) > 0 ) : ?>
	<div class="wprm-recipe-instructions-container">
		<div class="wprm-recipe-header"><h3><?php echo WPRM_Template_Helper::label( 'instructions' ); ?></h3></div>
		<?php foreach ( $instructions as $instruction_group ) : ?>
		<div class="wprm-recipe-instruction-group">
			<?php if ( $instruction_group['name'] ) : ?>
			<div class="wprm-recipe-group-name wprm-recipe-instruction-group-name"><h4><?php echo $instruction_group['name']; ?></h4></div>
			<?php endif; // Instruction group name. ?>
			<ol class="wprm-recipe-instructions">
				<?php foreach ( $instruction_group['instructions'] as $instruction ) : ?>
				<li class="wprm-recipe-instruction">
					<?php if ( $instruction['text'] ) : ?>
					<div class="wprm-recipe-instruction-text" itemprop="recipeInstructions"><?php echo wpautop( $instruction['text'] ); ?></div>
					<?php endif; // Instruction text. ?>
					<?php if ( $instruction['image'] ) : ?>
					<div class="wprm-recipe-instruction-image"><?php echo wp_get_attachment_image( $instruction['image'], 'thumbnail' ); ?></div>
					<?php endif; // Instruction image. ?>
				</li>
				<?php endforeach; // Instructions. ?>
			</ol>
		</div>
		<?php endforeach; // Instruction groups. ?>
	</div>
	<?php endif; // Instructions. ?>
	<?php if ( $recipe->notes() ) : ?>
	<div class="wprm-recipe-notes-container">
		<div class="wprm-recipe-header"><h3><?php echo WPRM_Template_Helper::label( 'notes' ); ?></h3></div>
		<?php echo $recipe->notes(); ?>
	</div>
	<?php endif; // Notes ?>
	<?php echo WPRM_Template_Helper::nutrition_label( $recipe->id() ); ?>
</div>
